<?php

namespace Paquette\GraphQL\Type;

use Paquette\User;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class UserType extends GraphQLType
{
    protected $attributes = [
        'name' => 'UserType',
        'description' => 'A type',
        'model' => User::class,
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::ID()),
            ],
            'email' => [
                'type' => Type::nonNull(Type::string()),
            ],
            'first_name' => [
                'type' => Type::nonNull(Type::string()),
            ],
            'last_name' => [
                'type' => Type::nonNull(Type::string()),
            ],
        ];
    }
}
