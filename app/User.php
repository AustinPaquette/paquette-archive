<?php

namespace Paquette;

use Balping\HashSlug\HasHashSlug;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens as Passport;
use Paquette\Amethyst\Traits\AmethystUserTrait as Amethyst;

class User extends Authenticatable
{
    use Amethyst, HasHashSlug, Notifiable, Passport, SoftDeletes;

    /**
     * Resource Database table
     *
     * @var string
     */
    protected $table = 'core_users';

    /**
     * The primary key of the resource database table
     *
     * @var string
     */
    public $primaryKey = 'increment_id';

    /**
     * Cast the Preferences object to an array
     *
     * @var array
     */
    protected $casts = [
        'preferences' => 'array',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'currency',
        'email',
        'first_name',
        'id',
        'last_name',
        'preferences',
        'plan_id',
        'password',
        'stripe_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'card_brand',
        'card_last_four',
        'password',
        'remember_token',
        'stripe_id',
    ];
}
