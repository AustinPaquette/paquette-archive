import * as React from 'react'
import * as ReactDOM from 'react-dom'
import Paquette from '../components/Paquette'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<Paquette />, div)
  ReactDOM.unmountComponentAtNode(div)
})
