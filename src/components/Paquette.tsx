import * as React from 'react'
import { ApolloProvider } from 'react-apollo'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import client from 'src/api/client'
import Core from './core'
import Amethyst from './products/amethyst'

export default class Paquette extends React.Component {
  public render() {
    return (
      <Router>
        <ApolloProvider client={client}>
          <React.Fragment>
            <Route path="/" exact={true} component={Core} />
            <Route path="/amethyst" component={Amethyst} />
          </React.Fragment>
        </ApolloProvider>
      </Router>
    )
  }
}
