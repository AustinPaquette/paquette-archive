import * as React from 'react'
import * as ReactDOM from 'react-dom'
import Paquette from './components/Paquette'
import './styles/index.css'

// Bind the Single-Page Application
ReactDOM.render(<Paquette />, document.getElementById('root') as HTMLElement)
