<!doctype html>
<html lang="{{ app()->getLocale() }}">
  <head>
    @include ('core.shared.head.first')
    @include ('core.shared.head.favicon')

    <title>
      {{ config('app.name', 'Paquette Application') }}
      @if(isset($current_app_name)) {{ $current_app_name }}@endif
      @if(isset($current_app_meta)) - {{ title_case($current_app_meta) }}@endif
    </title>

    <!-- CSRF Token -->
    <meta name="csrfToken" content="{{ csrf_token() }}">
    <meta name="token" content="{{ Session::token() }}">

    <!-- One Page App Helpers -->
    <meta name="fallback_title" content="{{ config('app.name', 'Paquette Application') }} @if(isset($current_app_name)){{ $current_app_name }}@endif @if(isset($current_app_meta))- {{ title_case($current_app_meta) }}@endif">
    <meta name="env" content="{{ App::environment() }}">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">

    @include ('core.shared.head.general')
    @yield ('base.assets.head')
  </head>

  <body class="paquette-body{{ Auth::check() ? ' logged-in' : null }}{{ Auth::check() && Auth::user()->staff ? ' is-staff' : null }}">
    <div id="paquette" class="app">
      @yield ('beforeContent')
      <Route>
        @yield ('content')
      </Route>
      @yield('afterContent')
    </div>

    @yield('beforeAssets')
    <aside class="assets">
      @yield ('prependAssets')
      <script src="{{ mix('js/manifest.js') }}" defer></script>
      <script src="{{ mix('js/vendor.js') }}" defer></script>
      <script src="{{ mix('js/app.js') }}" defer></script>
      @yield ('appendAssets')
    </aside>
    @yield ('afterAssets')
  </body>
</html>