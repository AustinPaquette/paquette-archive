@extends('auth')

@section('auth.assets.head')
  <meta name="authenticationErrors" content="{!! str_replace("\"", "'", $errors) !!}">
  <meta name="old_first_name" content="{{ old('first_name') }}">
  <meta name="old_last_name" content="{{ old('last_name') }}">
  <meta name="old_email" content="{{ old('email') }}">
@endsection

@section('content')
<div id="paquette-register"></div>
@endsection
