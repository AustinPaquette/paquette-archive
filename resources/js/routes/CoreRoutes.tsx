import React from 'react'
import { Route } from 'react-router'
import CoreHome from '../components/core/CoreHome'

export const CoreRoutes = () => (
  <>
    <Route exact path="/" component={CoreHome} />
  </>
)

export default CoreRoutes
