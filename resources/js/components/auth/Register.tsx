import React, { Component } from 'react'
import { Action, ActionGroup } from '@paquette/ui/react/actions'
import { Btn as Button } from '@paquette/ui/react/buttons'
import { Card, CardSection } from '@paquette/ui/react/cards'
import { Container } from '@paquette/ui/react/containers'
import {
  FormActions,
  FormAlert,
  FormControl,
  FormGroup,
  FormLabel,
  FormPassword,
} from '@paquette/ui/react/forms'
import { PaquetteWordmark as Wordmark } from '@paquette/ui/react/branding'
import zxcvbn from 'zxcvbn'

interface ReactComponentRegisterProps {}

interface ReactComponentRegisterState {
  csrf_token: string
  errors: any
  inputs: {
    first_name: string
    last_name: string
    email: string
  }
  password: {
    feedback: {
      suggestions: Array<string>
      warning: string
    }
    type: string | null
    value: string
    value_confirm: string
  }
}

export default class Register extends Component<
  ReactComponentRegisterProps,
  ReactComponentRegisterState
> {
  constructor(props: ReactComponentRegisterProps) {
    super(props)

    this.state = {
      csrf_token: '',
      errors: [],
      inputs: {
        first_name: '',
        last_name: '',
        email: '',
      },
      password: {
        feedback: {
          suggestions: [],
          warning: '',
        },
        type: null,
        value: '',
        value_confirm: '',
      },
    }
  }

  componentDidMount = () => {
    const token: HTMLMetaElement = document.head!.querySelector(
      'meta[name="csrfToken"]'
    ) as HTMLMetaElement

    const authenticationErrors: HTMLMetaElement = document.head!.querySelector(
      'meta[name="authenticationErrors"]'
    ) as HTMLMetaElement

    const errors = JSON.parse(authenticationErrors.content.replace(/'/g, '"'))

    this.setState({
      csrf_token: token.content as string,
      errors,
    })

    this.detectOldInputs()
  }

  detectOldInputs = () => {
    const first_name: HTMLMetaElement = document.head!.querySelector(
      'meta[name="old_first_name"]'
    ) as HTMLMetaElement
    const last_name: HTMLMetaElement = document.head!.querySelector(
      'meta[name="old_last_name"]'
    ) as HTMLMetaElement
    const email: HTMLMetaElement = document.head!.querySelector(
      'meta[name="old_email"]'
    ) as HTMLMetaElement

    this.setState(
      prevState =>
        ({
          ...prevState,
          inputs: {
            first_name: first_name.content,
            last_name: last_name.content,
            email: email.content,
          },
        } as any)
    )
  }

  handlePasswordInput = (event: Event) => {
    const { value }: HTMLInputElement = event.target as HTMLInputElement
    const { feedback } = zxcvbn(value)
    const type = (): string | null => {
      if (value.length < 6) {
        return null
      }

      if (feedback.warning.length) {
        return 'danger'
      }

      return null
    }

    this.setState({
      password: {
        ...this.state.password,
        feedback,
        type: type(),
        value,
      },
    })
  }

  handlePasswordAlert = () => {
    const { password } = this.state

    if (password.value.length < 6)
      return (
        <FormAlert
          text="Must be at least 6 characters."
          modifiers={{ color: password.type }}
        />
      )

    if (password.feedback.warning)
      return (
        <FormAlert
          text={password.feedback.warning}
          modifiers={{ color: password.type }}
        />
      )

    if (password.feedback.suggestions.length)
      return <FormAlert text={password.feedback.suggestions[0]} />

    return
  }

  handlePasswordConfirmationInput = (event: Event) => {
    const { value } = event.target as HTMLInputElement

    this.setState({
      password: {
        ...this.state.password,
        value_confirm: value,
      },
    })
  }

  handlePasswordConfirmationAlert = (): JSX.Element | void => {
    const { value, value_confirm } = this.state.password

    if (value.length < 6 || value_confirm.length < 6) return

    if (value === value_confirm)
      return <FormAlert text="Matches" modifiers={{ color: 'success' }} />
  }

  handleTextInput = (event: Event) => {
    const { name, value }: HTMLInputElement = event.target as HTMLInputElement

    this.setState(
      prevState =>
        ({
          ...prevState,
          inputs: {
            [name]: value,
          },
        } as any)
    )
  }

  render(): JSX.Element {
    const { csrf_token, errors, inputs, password } = this.state

    return (
      <Container>
        <form method="POST" action="/register">
          <a href="/">
            <Wordmark />
          </a>
          <Card className="register-form">
            <CardSection>
              <input type="hidden" name="_token" value={csrf_token} />

              <FormGroup>
                <FormLabel id="first_name" text="First Name" />
                <FormControl
                  id="first_name"
                  placeholder="John"
                  value={inputs.first_name}
                  onChange={this.handleTextInput}
                />
                {typeof errors.first_name !== 'undefined' &&
                errors.first_name.length ? (
                  <FormAlert
                    text={errors.first_name[0]}
                    modifiers={{ color: 'error' }}
                  />
                ) : (
                  undefined
                )}
              </FormGroup>

              <FormGroup>
                <FormLabel id="last_name" text="Last Name" />
                <FormControl
                  id="last_name"
                  placeholder="Doe"
                  value={inputs.last_name}
                  onChange={this.handleTextInput}
                />
                {typeof errors.last_name !== 'undefined' &&
                errors.last_name.length ? (
                  <FormAlert
                    text={errors.last_name[0]}
                    modifiers={{ color: 'error' }}
                  />
                ) : (
                  undefined
                )}
              </FormGroup>

              <FormGroup>
                <FormLabel id="email" text="Email" />
                <FormControl
                  id="email"
                  placeholder="name@domain.com"
                  value={inputs.email}
                  onChange={this.handleTextInput}
                />
                {typeof errors.email !== 'undefined' && errors.email.length ? (
                  <FormAlert
                    text={errors.email[0]}
                    modifiers={{ color: 'error' }}
                  />
                ) : (
                  undefined
                )}
              </FormGroup>

              <FormGroup>
                <FormLabel id="password" text="Password" />
                <FormPassword
                  id="password"
                  onChange={this.handlePasswordInput}
                />
                {!password.value.length &&
                typeof errors.password !== 'undefined' &&
                errors.password.length ? (
                  <FormAlert
                    text={errors.password[0]}
                    modifiers={{ color: 'error' }}
                  />
                ) : (
                  this.handlePasswordAlert()
                )}
              </FormGroup>

              <FormGroup
                hidden={this.state.password.value.length < 6 ? true : false}
              >
                <FormLabel id="password_confirmation" text="Confirm Password" />
                <FormControl
                  id="password_confirmation"
                  type="password"
                  placeholder="••••••••••••"
                  onChange={this.handlePasswordConfirmationInput}
                />
                {this.handlePasswordConfirmationAlert()}
              </FormGroup>

              <FormActions>
                <Button modifiers={{ color: 'primary', size: 'large' }}>
                  Register
                </Button>
              </FormActions>
            </CardSection>
          </Card>
          <ActionGroup className="links">
            <Action tag="a" href="/login">
              Login
            </Action>
          </ActionGroup>
        </form>
      </Container>
    )
  }
}
