import React from 'react'
import { Topbar } from '@paquette/ui/react/topbar'
import { NavLink } from 'react-router-dom'
import { PaquetteWordmark as Wordmark } from '@paquette/ui/react/branding'
import GlobalAccount from './GlobalAccount'

export const GlobalTopbar = () => (
  <Topbar
    logo={
      <NavLink to="/">
        <Wordmark />
      </NavLink>
    }
    navigation={
      <>
        <NavLink to="/">Home</NavLink>
      </>
    }
    account={<GlobalAccount />}
  />
)

export default GlobalTopbar
