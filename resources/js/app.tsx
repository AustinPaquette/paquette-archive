import React from 'react'
import { render } from 'react-dom'
import Paquette from './components/Paquette'
import Login from './components/auth/Login'
import Register from './components/auth/Register'
import geolocation from './api/geolocation'

/**
 * Process Geolocation Data
 */
geolocation()

/**
 * Bind the Core SPA
 */
if (document.getElementById('paquette')) {
  render(<Paquette />, document.getElementById('paquette') as HTMLElement)
}

/**
 * Display the Login Component
 */
if (document.getElementById('paquette-login')) {
  render(<Login />, document.getElementById('paquette-login') as HTMLElement)
}

/**
 * Display the Register Component
 */
if (document.getElementById('paquette-register')) {
  render(<Register />, document.getElementById(
    'paquette-register'
  ) as HTMLElement)
}
