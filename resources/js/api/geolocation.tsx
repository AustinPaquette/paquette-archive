import axios from 'axios'

export const handleGeoLocation = () => {
  const geolocationURI = 'https://ipapi.co/json'
  const localStoragePrefix = 'geolocation:'
  const fields = [
    'ip',
    'city',
    'region',
    'region_code',
    'country',
    'country_name',
    'continent_code',
    'in_eu',
    'postal',
    'latitude',
    'longitude',
    'timezone',
    'utc_offset',
    'country_calling_code',
    'currency',
    'languages',
    'asn',
    'org',
  ]

  /**
   * Check that all of the fields we expect have a value, otherwise return the ones missing
   * @param fieldKeys Array<string>
   * @returns Array<string>
   */
  const checkGeoLocationItems = (fieldKeys: Array<string>): Array<string> => {
    return fieldKeys.filter(key => {
      const value = localStorage.getItem(localStoragePrefix + key)
      return value ? undefined : key
    })
  }

  /**
   * Remove all of the fields
   * @param fieldKeys Array<string>
   * @returns void
   */
  const clearGeoLocationItems = (fieldKeys: Array<string>) => {
    fieldKeys.forEach(key => {
      localStorage.removeItem(key)
    })
  }

  /**
   * Fetch and store the geolocation data to localStorage
   * @returns void
   */
  const createGeoLocationItems = () => {
    axios.get(geolocationURI).then(response => {
      const geolocationData = response.data
      Object.keys(geolocationData).forEach((key: string) => {
        localStorage.setItem(localStoragePrefix + key, geolocationData[key])
      })
    })
  }

  /**
   * Consider what needs to be done when loaded
   * @returns void
   */
  const handle = () => {
    /**
     * Check for any missing key value pairs
     */
    const check = checkGeoLocationItems(fields)

    /**
     * If we're missing anything expected, remove existing ones and refetch
     */
    if (check.length) {
      clearGeoLocationItems(fields)
      createGeoLocationItems()
    }
  }

  /**
   * Initialize
   */
  handle()
}

export default handleGeoLocation
