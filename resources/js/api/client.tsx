import ApolloClient from 'apollo-boost'

/**
 * Authentication Details for Apollo Queries
 */
const token: HTMLMetaElement = document.head!.querySelector(
  'meta[name="csrfToken"]'
) as HTMLMetaElement

const client = new ApolloClient({
  uri: '/graphql',
  headers: {
    authorization: token ? token.content : '',
    'X-CSRF-TOKEN': token ? token.content : '',
  },
})

export default client
