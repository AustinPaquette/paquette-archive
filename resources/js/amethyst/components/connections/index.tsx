import React from 'react'
import { Card, CardGroup, CardSection } from '@paquette/ui/react/cards'
import { Container } from '@paquette/ui/react/containers'

export const index = () => (
  <Container className="amethyst-connections-index">
    <h1>Connections</h1>
    <CardGroup>
      <Card>
        <CardSection>Test</CardSection>
      </Card>
    </CardGroup>
  </Container>
)

export default index
