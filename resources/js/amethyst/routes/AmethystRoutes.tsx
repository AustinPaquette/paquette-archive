import React from 'react'
import { Route } from 'react-router'
import ConnectionsIndex from '../components/connections/index'
import ConnectionsCreate from '../components/connections/create'

export const AmethystRoutes = () => (
  <>
    <Route exact path="/amethyst/connections" component={ConnectionsIndex} />
    <Route
      exact
      path="/amethyst/connections/create"
      component={ConnectionsCreate}
    />
  </>
)

export default AmethystRoutes
