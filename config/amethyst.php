<?php

return [
    'stripe' => [
        'authorize_uri' => env('STRIPE_AUTHORIZE_URI', 'https://connect.stripe.com/oauth/authorize'),
        'token_uri' => env('STRIPE_TOKEN_URI', 'https://connect.stripe.com/oauth/token'),
        'mode' => env('STRIPE_MODE', 'test'),

        'live' => [
            'client_id' => env('STRIPE_LIVE_CLIENT_ID'),
            'publishable_key' => env('STRIPE_LIVE_PUBLISHABLE_KEY'),
            'secret_key' => env('STRIPE_LIVE_SECRET_KEY'),
        ],

        'test' => [
            'client_id' => env('STRIPE_TEST_CLIENT_ID'),
            'publishable_key' => env('STRIPE_TEST_PUBLISHABLE_KEY'),
            'secret_key' => env('STRIPE_TEST_SECRET_KEY'),
        ],
    ],
];
