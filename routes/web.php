<?php

Route::domain(env('APP_URL'))->group(function () {
    Auth::routes();

    Route::group(['middleware' => 'auth'], function () {
      Route::name('app')->any('{catchall}', 'PageController@app')->where('catchall', '(.*)');
    });
});
